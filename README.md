#Tracker
React application connected to Sewio system via WebSocket. It shows realtime position of tags and zones on a floorplan.
  
[Bitbucket repository](https://bitbucket.org/davidboron/tracker/)  
  
###Install
```
yarn install
```  
  
###Build
```
yarn run build
```  
  
###Prod
  
Application is supposed to be included and to run from  [Logger app](https://bitbucket.org/davidboron/logger/).
After successful build - copy files
* /dist/main.css
* /dist/main.js

into 
```
Logger/src/main/webapp/css/
Logger/src/main/webapp/js/
```

  
###Run dev
If you want to run development server 
```
yarn start
```
  
Dev server will be accesible on [http://localhost:8080/](http://localhost:8080/)


###Run with mock
Change 
 ```
 /config/index.js
 ```
to this code 
```
//import config from "./sewio";
import config from "./localhost";

export default config;
```