import React, {Component} from "react";
import PropTypes from 'prop-types';

class Client extends Component {

    constructor(props) {
        super(props);
        this.state = {
            tags: {},
            img: null
        };
    }

    componentDidMount() {
        this.connectToSocket();
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.building.name !== this.props.building.name) {
            this.closeSocket();
            // clear tags and reconnect to another building
            this.setState(() => ({tags: {}}));
            this.connectToSocket();
        }
    }

    componentWillUnmount() {
        this.closeSocket();
    }

    connectToSocket = () => {

        const {url, apiKey, building} = this.props;

        let ws = new WebSocket(url);
        this.ws = ws;

        console.log('Connecting to:', url, building.name);

        ws.onopen = () => {
            console.log('Connection successful');
            this.subscribeAllFeeds(ws, apiKey);
        };

        ws.onmessage = (response) => {
            let parsed = JSON.parse(response.data);
            this.setTagData(parsed.body);
        };

        this.loadImage(building.url);
    };

    closeSocket = () => {
        if (this.ws.readyState === WebSocket.OPEN) {
            console.log('Closing connection');
            this.ws.close();
        }
    };

    subscribeAllFeeds = (ws, apiKey) => {

        const {feeds} = this.props;

        console.log('Current feeds:', feeds);

        feeds.map((feed) => {
            let message = {
                "headers": {
                    "X-ApiKey": apiKey
                },
                "method": "subscribe",
                "resource": "/feeds/" + feed.id
            };

            ws.send(JSON.stringify(message));
        })
    };

    setTagData = (data) => {

        const {scale} = this.props.building;

        let coords = data.datastreams;

        let cx = coords[0].current_value * scale;
        let cy = coords[1].current_value * scale;

        let isInside = data.hasOwnProperty("zones");
        let fillColor = isInside ? "red" : "yellow";

        this.setState((prevState) => ({
            tags: {
                ...prevState.tags,
                [data.id]: {
                    coords: {
                        x: cx,
                        y: cy
                    },
                    fill: fillColor
                }
            }
        }))
    };

    loadImage = (url) => {
        let img = new Image();
        img.src = url;
        img.onload = ({target: img}) => {
            this.setState(() => ({
                img: {
                    width: img.width,
                    height: img.height,
                    src: img.src
                }
            }))
        }
    };

    renderTags = () => {
        const {tags} = this.state;
        return Object.keys(tags).map((index) => {
            let tag = tags[index];
            return <circle key={index} id="circle" cx={tag.coords.x} cy={tag.coords.y} r="10" stroke="green" strokeWidth="4" fill={tag.fill}/>
        });
    };

    renderZones = () => {
        const {zones} = this.props;
        const {scale} = this.props.building;
        return zones.map((zone) => {
            // parse and multiply by scale
            let parsedZone = zone.polygon.replace(/^POLYGON\(\((.*)\)\)$/, '$1').split(',');
            let points = parsedZone.map((item) => {
                return item.split(' ').map((pos) => {
                    return parseFloat(pos) * scale;
                }).join(' ');
            }).join(',');
            return <polygon key={zone.id} points={points}/>
        })
    };

    render() {

        const {img} = this.state;
        const {building} = this.props;

        return (
            <div>

                {img &&
                <svg width="1024" viewBox={`0 0 ${img.width} ${img.height}`} preserveAspectRatio="xMinYMin">
                    <image x={building.originX * -1} y={building.originY * -1} width="100%" href={img.src}/>
                    {this.renderZones()}
                    {this.renderTags()}
                </svg>
                }
            </div>
        );
    }
}

Client.propTypes = {
    url: PropTypes.string,
    apiKey: PropTypes.string,
    building: PropTypes.object,
    feeds: PropTypes.array,
    zones: PropTypes.array
};


export default Client;