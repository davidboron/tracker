import React, {Component} from "react";
import ReactDOM from "react-dom";
import "./css/styles.css";
import config from "../config";
import Client from "./Client";


class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            currentBuilding: null,
            buildings: [],
            zones: []
        }
    }

    componentDidMount() {
        this.getAllFeeds("http://" + config.server + "/sensmapserver/api/feeds");
    }

    getAllFeeds = (url) => {
        fetch(url)
            .then(function (response) {
                return response.json();
            })
            .then((response) => {
                this.feeds = response.results;
                let buildings = response.results.filter(item => item.type === "building");
                this.setState(() => ({
                    buildings: buildings
                }));
            });
    };

    setCurrentBuilding = (e, building) => {
        e.preventDefault();
        let currentBuilding = {
            ...building.plans[Object.keys(building.plans)[0]],
            id: building.id
        };
        this.setState(() => ({
            currentBuilding
        }));

        this.loadZones(currentBuilding);
    };

    filterFeedByBuilding = (building) => {
        return this.feeds.filter((item) => item.location.ele === building && item.type === "tag")
    };

    loadZones = (currentBuilding) => {

        fetch("http://" + config.server + "/sensmapserver/api/buildings/" + currentBuilding.id + "/plans/" + currentBuilding.name + "/zones")
            .then(function (response) {
                return response.json();
            })
            .then((response) => {
                this.setState(() => ({
                    zones: response
                }));
            });
    };

    render() {
        const {currentBuilding, buildings, zones} = this.state;

        return (
            <div>
                <p>Choose building:</p>
                <ul>
                    {buildings.map((item, index) => (
                        <a key={index} href="" onClick={e => this.setCurrentBuilding(e, item)}>
                            <li>{item.title}</li>
                        </a>
                    ))}
                </ul>
                <hr/>
                {currentBuilding &&
                <Client
                    url={"ws://" + config.server + ":" + config.port}
                    apiKey={config.apiKey}
                    building={currentBuilding}
                    feeds={this.filterFeedByBuilding(currentBuilding.name)}
                    zones={zones}
                />
                }
            </div>
        )
    }

}

ReactDOM.render(<App/>, document.getElementById("root"));